__author__ = 'helmbrecht'


import numpy as np
# matplotlib tk
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colorbar as cb
from pylab import imshow,show
from PIL import Image
import scipy.cluster as cluster
from scipy.misc import toimage
from sklearn import manifold, datasets
#from skimage.viewer import viewers


from scipy import stats
import scipy as sc
import scipy.ndimage as ndimage
from skimage import io
import skimage as sk
from skimage import transform
import matplotlib.cm as cmap
from All_functions_needed_Anna import *
from filepicker import *



####################multitiff data are imported and converted to 8bit array#############################################
print 'Pick Imaging File (8bit, mutltitiff)'
plt.switch_backend('tkagg')
plt.get_backend()

filepath = pickfiles()

for filex, file in enumerate(filepath,0):
    print file

    print 'DATA Loading...'
    img = io.MultiImage(file,conserve_memory=False)
    frames=sk.img_as_ubyte(img)
    print 'Data loaded'
    ##################### Resizing the frames - Downsampling by a factor of 2 ############################################

    resize_factor = 2
    test = sk.transform.downscale_local_mean(frames[0,:,:], (resize_factor, resize_factor))
    frames_ds = np.ones((len(frames[:,0,0]),len(test[:,0]), len(test[0,:])))
    for i in range(len(frames[:,0,0])):
        frames_ds[i,:,:] = sk.transform.downscale_local_mean(frames[i,:,:], (resize_factor, resize_factor))
    # plt.imshow(frames_ds[0])
    # plt.colorbar()
    # # plt.show()
    #
    # plt.figure(4)
    # plt.imshow(frames[0])
    # plt.colorbar()
    # plt.show()
    ########################## AVEREGING OVER REPEATS ####################################################################
    # from Average_over_repeats_for_Anna_END import *

    # num_frames = raw_input('Enter recording length in frames for the complete stimulus, such as 1190')
    num_frames = len(frames[:,0,0])

    # from create_regressors_for_Anna import *
    reg_signal = read_and_downsample_xlsx(num_frames)

    timepoints, reg_signal_av, third = average_over_3_rep(reg_signal)

    # plt.plot(reg_signal_av)
    # # plt.show()
    #
    # plt.figure(2)
    # plt.plot(reg_signal)
    # plt.show()

    # print timepoints

    frames_third = np.reshape(frames_ds[timepoints[0]:timepoints[1],:,:],(3,third, len(frames_ds[0,:,0]), len(frames_ds[0,0,:])))

    frames_ds_av = np.mean(frames_third, axis=0)


    ########################################################################################################################
    '''REGRESSOR:'''

    all_regr, threshold = create_regr(reg_signal_av)

    'Find all regressors needed for Anna'

    stim_on = np.where(reg_signal_av > threshold)[0][0]
    stim_off = np.where(reg_signal_av > threshold)[0][-1]
    all_regr_bin = np.ones([len(all_regr),8])
    for i in range(len(all_regr)):
        all_regr_bin[i] = downsample(all_regr[i][stim_on:stim_off],8)



    all_regr_bin_list = all_regr_bin.tolist()
    main_regressor = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    wanted_regressors = [[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                         [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0],
                         [1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0],
                         [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                         [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
                         [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0],
                         [0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0],
                         [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]]
    regx = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0]
    reg_number = np.ones(len(wanted_regressors))
    all_regr_needed = np.ones([8, np.shape(all_regr)[1]])
    for i in range(len(wanted_regressors)):
        reg_number[i] = all_regr_bin_list.index(wanted_regressors[i])
        # main_reg_number = all_regr_bin_list.index(main_regressor)
        regxn = all_regr_bin_list.index(regx)
        all_regr_needed[i] = all_regr[reg_number[i]]

    #######################################################################################################################
    Width= len(frames_ds_av[0,:,0])
    Height= len(frames_ds_av[0,0,:])
    Samples= len(frames_ds_av[:,0,0])
    print  Samples,Width,Height

    ######################reshaping the data################################################################################
    series=np.empty((len(frames_ds_av[:,0,0]),Width*Height))
    series=np.reshape(frames_ds_av,(len(frames_ds_av[:,0,0]),Width*Height))

    ####################scoring pixel according to the intensity distribution###############################################
    print 'ZSCORE Calculation...'
    Zscorex=np.empty((Width*Height))
    Zscore2=np.empty((Width*Height))
    for i in range(Width*Height):
        Zscorex[i]=sc.stats.mstats.skewtest(series[:, i])[0]
        Zscore2[i]=sc.stats.mstats.skewtest(series[:, i])[1]
    Zscore=np.reshape(Zscorex, (Width, Height))
    Zscore2=np.reshape(Zscore2, (Width, Height))
    print 'Zscore calculated'

    plt.imshow(Zscore,vmin=0, vmax=10)
    plt.colorbar()
    plt.show()

    Z_thresh = raw_input('Enter threshold')
    Z_thresh = float(Z_thresh)



    '''Create Mask to select only active pixels and store them'''

    xmask = np.ma.make_mask(Zscore, copy=True, shrink=True, dtype=np.bool)
    maskx = np.reshape(xmask, (Width*Height))

    masks = np.ma.make_mask(series, copy=True, shrink=True, dtype=np.bool)

    for i in range(len(maskx)):
        # if Zscorex[i] < 11:
        if Zscorex[i] > Z_thresh:
            # print i
            maskx[i] = False
            masks[:,i] = False
        else:
            maskx[i] = True
            masks[:,i] = True

    series = np.ma.masked_array(series, mask=masks)
    series.harden_mask()


    ######################### DFF Calcultaion ##############################################################################

    print 'DFF Calculation...'
    DFF_series=np.zeros((len(frames_ds_av[:,0,0]),Width*Height))
    DFF_series = np.ma.masked_array(DFF_series, mask=masks)
    # DFF_series = np.ma.make_mask(DFF_series, copy=True, shrink=True, dtype=np.bool)
    base=np.ones(len(series[0]))
    s=0
    for i in range(len(series[0])):
        if not series[:,i].mask.any() == True:
            # print i
            s=ndimage.filters.percentile_filter(series[:,i],15,size=(len(series[:,1])),mode='reflect')
            s=np.mean(s)
            # print s
            #DFFseries[:,i]=(series[:,i]-base[i])/base[i]
            base[i]=s+1
            DFF_series[:,i]=(series[:,i]-base[i])/base[i]
    print 'DFF calculated'
    DFF_image=np.zeros(((len(frames_ds_av[:,0,0]),Width,Height)))
    DFF_image= np.reshape(DFF_series,(len(frames_ds_av[:,0,0]),Width,Height))
    DFFseries = DFF_series

    ########################################################################################################################
    ''' REGRESSOR_ANALYSIS'''
    ########################################################################################################################

    ''' AGAINST MAIN REGRESSOR'''
    #
    # PCf_main=np.ones(Width*Height)
    # PCp_main=np.ones(Width*Height)
    # # for reg in range(len(all_regr)):
    #     # s= sc.stats.pearsonr(all_regr[reg], series[200])[0]
    #     # PCf=np.empty((Width*Height,1))
    #     # PCp=np.empty((Width*Height,1))
    #     # print 'Regressor: ', reg
    # for i in range(0, len(DFFseries[0,:])):
    #     # if i in range(100, len(series[0,:]), 100):
    #     # # print z/
    #     #     print i
    #     PCf_main[i]=sc.stats.pearsonr(all_regr[main_reg_number], DFFseries[:,i])[0]
    #     PCp_main[i]=sc.stats.pearsonr(all_regr[main_reg_number], DFFseries[:,i])[1]
    #     # print PCp[i]
    #     if PCp_main[i]>0.9:
    #         PCf_main[i]=0

    """SIMPLE REGRESSOR_BASED ANALYSIS FOR ALL REGRESSORS"""
    '''NEW'''
    PCf=np.ones((len(all_regr),Width*Height))
    maskp = np.ma.make_mask(PCf, copy=True, shrink=True, dtype=np.bool)
    for i in range(len(maskx)):
        if Zscorex[i] >Z_thresh:
            maskp[:,i] = False
        else:
            maskp[:,i] = True
    maskp[:, ]

    PCf = np.ma.masked_array(PCf, mask=maskp)
    PCp=np.ones((len(all_regr),Width*Height))
    PCp = np.ma.masked_array(PCp, mask=maskp)
    for reg in range(len(all_regr)):
        # s= sc.stats.pearsonr(all_regr[reg], series[200])[0]
        # PCf=np.empty((Width*Height,1))
        # PCp=np.empty((Width*Height,1))
        print 'Regressor: ', reg
        for i in range(0, len(DFFseries[0,:])):
            if not DFFseries[:,i].mask.any() == True:
                # if i in range(100, len(series[0,:]), 100):
                # # print z/
                #     print i
                PCf[reg,i]=sc.stats.pearsonr(all_regr[reg], DFFseries[:,i])[0]
                PCp[reg,i]=sc.stats.pearsonr(all_regr[reg], DFFseries[:,i])[1]
                # print PCp[i]
                if PCp[reg,i]>0.9:
                    PCf[reg,i]=0

    # for i in range(len(all_regr)):
    #     x = np.where(PCf[i].mask == False)
    #     print i
    #     if x[0]:
    #         print 'yes'
    #
    # for i in range(len(all_regr)):
    #     print i
    #     if not DFFseries[:,i].mask.any() == True:
    #         print 'value found'

    ########################################################################################################################
    """ CALCULATE BEST REGRESSOR FOR EACH PIXEL"""
    print 'Calculate best regressor'
    def column(matrix, i):
        return [row[i] for row in matrix]

    for best in range(np.shape(PCf)[1]):
        # if best in range(100, np.shape(PCf)[1], 100):
        #     print best
        ind = np.argmax(column(PCf, best))
        replace = range(len(PCf[:,0]))
        replace.pop(ind)
        # test3 = [x for x in test2 if x != 1]
        np.put(PCf[:,best], replace, [0])
    print 'Best regressor calculated'


    ########################################################################################################################
    """ REDUCE NOISE: TAKE ONLY HIGHLY CORRELATED PIXELS WITH p-value < 0.1 """
    print 'Reduce Noise'
    for u in range(np.shape(PCp)[0]):
        for k in range(np.shape(PCp)[1]):
            # if PCp[u][k] > 0.01:
            if PCp[u][k] > 0.1:
                PCf[u][k] = 0
    print 'Noise reduced'

    ########################################################################################################################
    """ NICE FINAL PLOTS """
    print 'PLOTS'

    PCf_threshold = 0.4     #"""0.5 works quite well"""

    for reg in range(len(reg_number)):
        PCf_toplot=np.reshape(PCf[reg_number[reg]],(Width,Height,1))
        # PCp_toplot=np.reshape(PCp[reg_number[reg]],(Width,Height,1))

        grid_size = (4, 4)
        fig = plt.figure(reg, figsize=[8,8])
        colorm = [cmap.Reds]
        plt.subplot2grid(grid_size, (0, 0), rowspan=1, colspan=4)
        plt.plot(all_regr[reg_number[reg]], 'k', lw=3)
        plt.ylim([0,5])
        plt.xlim([0,len(all_regr[0])])
        ax = plt.subplot2grid(grid_size, (2, 0), rowspan=5, colspan=4)
        plt.imshow(np.mean(frames_ds_av[:,:,:], axis=0)*10, cmap=cmap.Greys_r, vmin=0, vmax=200)
        PCf_toplot[PCf_toplot == 0.0] = np.nan
        plt.imshow(PCf_toplot[:,:,0],cmap=colorm[0], vmin=0,vmax=0.7, aspect=1)
        ax.autoscale(False)
        plt.colorbar()
        mean_f, contours = ROIs_and_Plot(frames_ds_av, PCf, PCf_toplot, Width, Height, reg_number[reg], PCf_threshold)
        for n, contour in enumerate(contours):
            plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
        plt.subplot2grid(grid_size, (1, 0), rowspan=1, colspan=4)
        plt.plot(mean_f, 'k', lw=3)
        # fig.savefig(filepath.replace('.tif', '_regressor_cells.tif'))
    plt.show()

    """ALL FOUND REGRESSOR CELLS"""

    PCf_threshold = 0.4

    reg_to_plot = []
    for reg in range(len(all_regr)):
        # print reg
        PCf_toplot=np.reshape(PCf[reg],(Width,Height,1))
        PCf_toplot[PCf_toplot == 0.0] = np.nan
        mean_f, contours = ROIs_and_Plot(frames_ds_av, PCf, PCf_toplot, Width, Height,reg, PCf_threshold)
        if contours:
            print 'got_regressor', reg
            reg_to_plot.append(reg)
    print 'Done'

    for i, reg in enumerate(reg_to_plot):
        PCf_toplot=np.reshape(PCf[reg],(Width,Height,1))
        # PCp_toplot=np.reshape(PCp[reg_number[reg]],(Width,Height,1))

        grid_size = (4, 4)
        plt.figure(reg, figsize=[8,8])
        colorm = [cmap.Reds]
        plt.subplot2grid(grid_size, (0, 0), rowspan=1, colspan=4)
        plt.plot(all_regr[reg], 'k', lw=3)
        plt.ylim([0,5])
        plt.xlim([0,len(all_regr[0])])
        ax = plt.subplot2grid(grid_size, (2, 0), rowspan=5, colspan=4)
        plt.imshow(np.mean(frames_ds_av[:,:,:], axis=0)*10, cmap=cmap.Greys_r, vmin=0, vmax=200)
        PCf_toplot[PCf_toplot == 0.0] = np.nan
        plt.imshow(PCf_toplot[:,:,0],cmap=colorm[0], vmin=0,vmax=0.7, aspect=1)
        ax.autoscale(False)
        plt.colorbar()
        mean_f, contours = ROIs_and_Plot(frames_ds_av, PCf, PCf_toplot, Width, Height,reg, PCf_threshold)
        for n, contour in enumerate(contours):
            plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
        plt.subplot2grid(grid_size, (1, 0), rowspan=1, colspan=4)
        plt.plot(mean_f, 'k', lw=3)
        plt.xlim([0,len(all_regr[0])])
    plt.show()


###########################################################################################################################
#
#     '''TEST'''
#     PCf_toplot=np.reshape(PCf[regxn],(Width,Height,1))
#     # PCp_toplot=np.reshape(PCp[reg_number[reg]],(Width,Height,1))
#
#     grid_size = (4, 4)
#     plt.figure(regxn, figsize=[8,8])
#     colorm = [cmap.Reds]
#     plt.subplot2grid(grid_size, (0, 0), rowspan=1, colspan=4)
#     plt.plot(all_regr[regxn], 'k', lw=3)
#     plt.ylim([0,5])
#     plt.xlim([0,len(all_regr[0])])
#     ax = plt.subplot2grid(grid_size, (1, 0), rowspan=5, colspan=4)
#     plt.imshow(np.mean(frames_ds_av[:,:,:], axis=0)*10, cmap=cmap.Greys_r, vmin=0, vmax=200)
#     PCf_toplot[PCf_toplot == 0.0] = np.nan
#     plt.imshow(PCf_toplot[:,:,0],cmap=colorm[0], vmin=0,vmax=0.7, aspect=1)
#     ax.autoscale(False)
#     plt.colorbar()
#     plt.show()
#
# ''' FUNCTION TO PLOT ROIS'''
# def ROIs_and_Plot(PCf_toplot, Width, Height,reg):
#     PCf_toplot[np.isnan(PCf_toplot)] = 0.0
#     PCf_toplot=np.reshape(PCf[reg],(Width,Height,1))
#     from skimage import measure
#     test=np.reshape(PCf_toplot,(Width,Height))
#
#     contours = measure.find_contours(test.data, 0.5, fully_connected='high')
#     # for n, contour in enumerate(contours):
#     #     plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
#     # plt.show()
#
#     xs = []
#     ys = []
#     for i in range(Width):
#         for j in range(Height):
#             for k in range(len(contours)):
#                 if point_in_poly(i, j, contours[k]):
#                     xxs = [i]
#                     yys = [j]
#                     xs.append(xxs)
#                     ys.append(yys)
#                     # xyz.append(xyzz)
#
#     f = frames_ds_av[:,xs,ys]
#     mean_f = np.mean(f,axis=1)
#     return mean_f, contours
#
#
#
#     PCf_toplot=np.reshape(PCf[regxn],(Width,Height,1))
#     # PCp_toplot=np.reshape(PCp[reg_number[reg]],(Width,Height,1))
#
#     grid_size = (4, 4)
#     plt.figure(regxn, figsize=[8,8])
#     colorm = [cmap.Reds]
#     plt.subplot2grid(grid_size, (0, 0), rowspan=1, colspan=4)
#     plt.plot(all_regr[regxn], 'k', lw=3)
#     plt.ylim([0,5])
#     plt.xlim([0,len(all_regr[0])])
#     ax = plt.subplot2grid(grid_size, (2, 0), rowspan=4, colspan=4)
#     plt.imshow(np.mean(frames_ds_av[:,:,:], axis=0)*10, cmap=cmap.Greys_r, vmin=0, vmax=200)
#     PCf_toplot[PCf_toplot == 0.0] = np.nan
#     plt.imshow(PCf_toplot[:,:,0],cmap=colorm[0], vmin=0,vmax=0.7, aspect=1)
#     ax.autoscale(False)
#     plt.colorbar()
#     mean_f, contours = ROIs_and_Plot(PCf_toplot, Width, Height,regxn)
#     for n, contour in enumerate(contours):
#         plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
#     plt.subplot2grid(grid_size, (1, 0), rowspan=1, colspan=4)
#     plt.plot(mean_f, 'k', lw=3)
#
#     plt.show()

    # for reg in range(len(reg_number)):
    #     PCf_toplot=np.reshape(PCf[reg_number[reg]],(Width,Height,1))
    #     # PCp_toplot=np.reshape(PCp[reg_number[reg]],(Width,Height,1))
    #
    #     grid_size = (4, 4)
    #     plt.figure(reg, figsize=[8,8])
    #     colorm = [cmap.Reds]
    #     plt.subplot2grid(grid_size, (0, 0), rowspan=1, colspan=4)
    #     plt.plot(all_regr[reg_number[reg]], 'k', lw=3)
    #     plt.ylim([0,5])
    #     plt.xlim([0,len(all_regr[0])])
    #     ax = plt.subplot2grid(grid_size, (1, 0), rowspan=5, colspan=4)
    #     plt.imshow(np.mean(frames_ds_av[:,:,:], axis=0)*10, cmap=cmap.Greys_r, vmin=0, vmax=200)
    #     PCf_toplot[PCf_toplot == 0.0] = np.nan
    #     plt.imshow(PCf_toplot[:,:,0],cmap=colorm[0], vmin=0,vmax=0.7, aspect=1)
    #     ax.autoscale(False)
    #     plt.colorbar()
    # plt.show()