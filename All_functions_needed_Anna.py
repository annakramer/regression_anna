__author__ = 'helmbrecht'

import numpy as np
import csv
import xlrd
from filepicker import*
import numpy as np
#import matplotlib.pyplot as plt
import itertools
from operator import itemgetter
from itertools import groupby
from skimage import measure


#plt.plot(reg_signal)
# plt.show()
'''DOWNSAMPLE REGRESSOR FROM FEMTONICS'''

def downsample(input, length):
    t = np.linspace(0, len(input), length)
    output = np.interp(t, np.arange(len(input)), input)
    return output

def read_and_downsample_xlsx(Samples):
    print 'Pick Signal File (Femtonics, Excel_file)'
    filepath = pickfile()

    wb = xlrd.open_workbook(filepath)
    sh = wb.sheet_by_index(0)
    # colA = sh.col_values(0) # Make column 1 into a Python list
    colB = sh.col_values(1) # Make column 2 into a Python list
    # colA.pop(0) # Delete 1st row (header)
    colB.pop(0) # Delete 1st row (header)
    # plt.plot(colA,colB,'o')
    # plt.show()
    reg_signal = downsample(colB, Samples)
    return reg_signal

'''REGRESSOR:'''

def powerset2(s):
    x = len(s)
    power = []
    for i in range(1 << x):
        power.append([s[j] for j in range(x) if (i & (1 << j))])
    return power

def create_regr(reg_signal):

    #plt.figure();plt.plot(reg_signal)
    #plt.show()

    # threshold = raw_input('Enter threshold for regressor_generation (eg.: 4)')
    threshold = 4

    regr_coord = np.where(reg_signal > float(threshold)) #1.5

    regr_single = []
    for k, g in groupby(enumerate(np.ndarray.tolist(regr_coord[0])), lambda (i,x):i-x):
        regr_single.append(map(itemgetter(1), g))

    list_regressors = powerset2(regr_single)

    all_regressors = []
    for i in range(len(list_regressors)):
        if i == 0:
            mergedlist = []
        else:
            mergedlist = []
            for j in range(len(list_regressors[i])):
                mergedlist.extend(list_regressors[i][j])
        all_regressors.append(mergedlist)

    all_regr = np.zeros([len(all_regressors),len(reg_signal)] )
    for j in range(len(all_regressors)):
        # all_regr[j] = np.zeros(len(whole_TeO))
        np.put(all_regr[j], all_regressors[j], [1])

    return all_regr, float(threshold)


''' AVERAGE THE THREE REPEATS'''

def average_over_3_rep(reg_signal):
    stim_on = np.where(reg_signal > 0)[0][0]
    stim_off = np.where(reg_signal > 0)[0][-1]

    # plt.plot(reg_signal[stim_on-1:stim_off+2])
    # plt.show()

    '''FIND INTER_STIM_INTERVAL'''
    isi= np.where(reg_signal >0)[0]

    diff = np.ones(len(isi))
    for i in range(len(isi)):
        if i < len(isi)-1:
            diff[i] = isi[i+1]-isi[i]

    inter_stim_interv = np.max(diff)-2
    '''ADD HALF OF ISI TO EACH SIDE'''
    addings = int(np.round(inter_stim_interv/2))

    '''DIVIDE REG_SIGNAL INTO 3 PARTS'''
    third = len(reg_signal[stim_on-1-addings:stim_off+2+addings])/3

    mismatch = len(reg_signal[stim_on-1-addings:stim_off+2+addings]) - third*3
    if mismatch == 2:
        reg_signal_ad = reg_signal[stim_on-2-addings:stim_off+2+addings]
        third = third+1
        timep1 = stim_on-2-addings
        timep2 = stim_off+2+addings
    elif mismatch == 1:
        reg_signal_ad = reg_signal[stim_on-2-addings:stim_off+3+addings]
        third = third+1
        timep1 = stim_on-2-addings
        timep2 = stim_off+3+addings
    else:
        reg_signal_ad = reg_signal[stim_on-1-addings:stim_off+2+addings]
        timep1 = stim_on-1-addings
        timep2 = stim_off+2+addings

    timepoints = [timep1,timep2]
    reg_signal_third = np.reshape(reg_signal_ad,(3,third))

    reg_signal_av = np.mean(reg_signal_third, axis=0)

    return timepoints, reg_signal_av, third


''' FUNCTION TO PLOT ROIS'''

def point_in_poly(x, y, poly):
    """Calculates if a point is in the polygon"""
    n = len(poly)
    inside = False

    p1x, p1y = poly[0]
    for i in range(n+1):
        p2x, p2y = poly[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x, p1y = p2x, p2y

    return inside


def polygon_area(polygon, signed=False):
    """Calculate the signed area of non-self-intersecting polygon

    Input
        polygon: Numeric array of points (longitude, latitude). It is assumed
                 to be closed, i.e. first and last points are identical
        signed: Optional flag deciding whether returned area retains its sign:
                If points are ordered counter clockwise, the signed area
                will be positive.
                If points are ordered clockwise, it will be negative
                Default is False which means that the area is always positive.
    Output
        area: Area of polygon (subject to the value of argument signed)
    """

    # Make sure it is numeric
    P = np.array(polygon)

    # Check input
    msg = ('Polygon is assumed to consist of coordinate pairs. '
           'I got second dimension %i instead of 2' % P.shape[1])
    assert P.shape[1] == 2, msg

    msg = ('Polygon is assumed to be closed. '
           'However first and last coordinates are different: '
           '(%f, %f) and (%f, %f)' % (P[0, 0], P[0, 1], P[-1, 0], P[-1, 1]))
    assert np.allclose(P[0, :], P[-1, :]), msg

    # Extract x and y coordinates
    x = P[:, 0]
    y = P[:, 1]

    # Area calculation
    a = x[:-1] * y[1:]
    b = y[:-1] * x[1:]
    A = np.sum(a - b) / 2.

    # Return signed or unsigned area
    if signed:
        return A
    else:
        return abs(A)


def ROIs_and_Plot(frames_ds_av, PCf, PCf_toplot, Width, Height, reg, PCf_threshold):
    PCf_toplot[np.isnan(PCf_toplot)] = 0.0
    PCf_toplot=np.reshape(PCf[reg],(Width,Height,1))
    test=np.reshape(PCf_toplot,(Width,Height))

    contours = measure.find_contours(test.data, PCf_threshold, fully_connected='high')
    # for n, contour in enumerate(contours):
    #     plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
    # plt.show()

    xs = []
    ys = []
    for i in range(Width):
        for j in range(Height):
            for k in range(len(contours)):
                if point_in_poly(i, j, contours[k]):
                    xxs = [i]
                    yys = [j]
                    xs.append(xxs)
                    ys.append(yys)
                    # xyz.append(xyzz)

    f = frames_ds_av[:,xs,ys]
    mean_f = np.mean(f,axis=1)
    return mean_f, contours


### TEST ###################
Zthresh = 7

def ROIs_and_Plot_2(Zscore, Zthresh):
    # PCf_toplot[np.isnan(PCf_toplot)] = 0.0
    # PCf_toplot=np.reshape(PCf[reg],(Width,Height,1))
    # test=np.reshape(PCf_toplot,(Width,Height))
    Width = np.shape(Zscore)[0]
    Height = np.shape(Zscore)[1]

    contours = measure.find_contours(Zscore, Zthresh, fully_connected='high')
    # print 'DONE'
    # for n, contour in enumerate(contours):
    #     plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
    # plt.show()



    xs = []
    ys = []
    for i in range(Width):
        for j in range(Height):
            for k in range(len(contours)):
                if point_in_poly(i, j, contours[k]):
                    xxs = [i]
                    yys = [j]
                    xs.append(xxs)
                    ys.append(yys)
                    # xyz.append(xyzz)

    # f = frames_ds_av[:,xs,ys]
    # mean_f = np.mean(f,axis=1)
    print 'done'
    return contours




# testing = ROIs_and_Plot_2(Zscore, Zthresh)
# #
# #
#
# Zthresh = 8
# contours = measure.find_contours(Zscore, Zthresh, fully_connected='high')
# plt.imshow(np.mean(frames_ds_av[:,:,:], axis=0)*10, cmap=cmap.Greys_r, vmin=0, vmax=200)
# for n, contour in enumerate(contours):
#     plt.plot(contour[:, 1], contour[:, 0], linewidth=2)
#
# plt.figure(10)
# plt.imshow(Zscore,vmin=0, vmax=10)
# plt.colorbar()
# plt.show()